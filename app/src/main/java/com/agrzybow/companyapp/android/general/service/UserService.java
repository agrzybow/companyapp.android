package com.agrzybow.companyapp.android.general.service;

import android.content.Context;
import androidx.preference.PreferenceManager;
import com.agrzybow.companyapp.android.general.model.UserEntity;
import com.agrzybow.companyapp.android.general.model.api.UserAPI;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

import java.util.ArrayList;
import java.util.List;

public class UserService {
    public static final String CLIENT_ENTITY = "user.object";
    public static final String CLIENT_CREDENTIALS = "user.credentials";
    public static final String CLIENT_USERNAME = "user.username";

    private String clientUsername;
    private UserAPI userAPI;

    private List<Disposable> disposableList = new ArrayList<>();

    public UserService(Context context, String clientUsername) {
        userAPI = RetrofitClientService.getInstance(PreferenceManager.getDefaultSharedPreferences(context).getString(UserService.CLIENT_CREDENTIALS, null)).getService(UserAPI.class);
        this.clientUsername = clientUsername;
    }

    public UserService(Context context, String clientUsername, String credentials) {
        userAPI = RetrofitClientService.getInstance(credentials).getService(UserAPI.class);
        this.clientUsername = clientUsername;
    }

    public void saveToken(String token, Consumer<String> consumer) {
        disposableList.add(userAPI.saveToken(clientUsername, token)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(consumer, Throwable::printStackTrace));
    }

    public void checkResetPassword(Consumer<Boolean> consumer) {
        disposableList.add(userAPI.checkIfPasswordExpired(clientUsername)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(consumer, Throwable::printStackTrace));
    }

    public void changePassword(String password, Consumer<Boolean> consumer) {
        disposableList.add(userAPI.changePassword(clientUsername, password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(consumer, Throwable::printStackTrace));
    }

    public void login(Consumer<UserEntity> consumer, Consumer<Throwable> handleError) {
        disposableList.add(userAPI.login(clientUsername)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(consumer, handleError));
    }

    public void disposeService() {
        disposableList.forEach(Disposable::dispose);
    }
}
