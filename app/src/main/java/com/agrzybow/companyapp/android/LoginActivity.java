package com.agrzybow.companyapp.android;

import android.app.Activity;
import android.content.*;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.*;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import androidx.preference.PreferenceManager;
import com.agrzybow.R;
import com.agrzybow.companyapp.android.general.activity.ResetPasswordActivity;
import com.agrzybow.companyapp.android.general.activity.WorkspaceActivity;
import com.agrzybow.companyapp.android.general.model.UserEntity;
import com.agrzybow.companyapp.android.general.service.UserService;
import com.agrzybow.companyapp.android.quizmanagement.activity.QuizActivity;
import com.agrzybow.companyapp.android.general.service.CustomFirebaseMessagingService;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import okhttp3.Credentials;
import retrofit2.HttpException;

import java.net.ConnectException;

import static com.agrzybow.companyapp.android.general.service.UserService.*;


/**
 * Aktywność odpowiedzialna za proces logowania do aplikacji
 */
public class LoginActivity extends AppCompatActivity {
    private UserService userService;

    private TextInputEditText mUsernameView;
    private TextInputEditText mPasswordView;
    private Button mSignInButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mUsernameView = findViewById(R.id.username);
        mPasswordView = findViewById(R.id.password);

        mSignInButton = findViewById(R.id.sign_in_button);
        mSignInButton.setOnClickListener(view -> {
            attemptLogin();
        });

        String username = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString(CLIENT_USERNAME, null);
        String credentials = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString(CLIENT_CREDENTIALS, null);
        if (username != null && credentials != null) {
            login(username, credentials);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        FirebaseApp.initializeApp(getApplicationContext());
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(task -> {
                    if (!task.isSuccessful()) {
                        Log.w("Login", "getInstanceId failed", task.getException());
                        return;
                    }

                    // Get new Instance ID token
                    PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit()
                            .putString(CustomFirebaseMessagingService.FIREBASE_TOKEN, task.getResult().getToken())
                            .apply();
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case ResetPasswordActivity.ACTIVITY_CODE:
                if (resultCode == Activity.RESULT_OK) {
                    startQuizActivity();
                }
                break;
            case QuizActivity.ACTIVITY_CODE:
                if (resultCode == Activity.RESULT_OK) {
                    startMainActivity();
                }
                break;
            default:
                break;
        }
    }

    private void showError(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    private void attemptLogin() {
        mUsernameView.setError(null);
        mPasswordView.setError(null);

        String username = mUsernameView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (!isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        if (!isUsernameValid(username)) {
            mUsernameView.setError(getString(R.string.error_invalid_email));
            focusView = mUsernameView;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            mSignInButton.setEnabled(false);
            login(username, Credentials.basic(username, password));
        }
    }

    private void saveCredentials(UserEntity userEntity, String username, String credentials) {
        PreferenceManager.getDefaultSharedPreferences(this).edit()
                .putString(CLIENT_USERNAME, username)
                .putString(CLIENT_CREDENTIALS, credentials)
                .putString(CLIENT_ENTITY, new Gson().toJson(userEntity))
                .apply();
    }

    private void login(String username, String credentials) {
        userService = new UserService(getApplicationContext(), username, credentials);
        userService.login(response -> {
            saveCredentials(response, username, credentials);
            FirebaseMessaging.getInstance().setAutoInitEnabled(true);
            sendTokenIfPresent();
            startResetPasswordActivity();
            mSignInButton.setEnabled(true);
        }, error -> {
            if (error instanceof HttpException) {
                HttpException httpException = (HttpException) error;

                if (httpException.code() == 403) {
                    showError(getResources().getString(R.string.permission_denied));
                } else if (httpException.code() == 401) {
                    showError(getResources().getString(R.string.error_authenticated));
                }
            }
            if (error instanceof ConnectException) {
                showError(getResources().getString(R.string.error_server_connecting));
            }
            error.printStackTrace();
            mSignInButton.setEnabled(true);
        });
    }

    private void sendTokenIfPresent() {
        String token = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString(CustomFirebaseMessagingService.FIREBASE_TOKEN, null);
        if(token != null) {
            userService.saveToken(token, e -> {

            });
        }
    }

    private void startResetPasswordActivity() {
        startActivityForResult(new Intent(getApplicationContext(), ResetPasswordActivity.class), ResetPasswordActivity.ACTIVITY_CODE);
    }

    private void startQuizActivity() {
        startActivityForResult(new Intent(getApplicationContext(), QuizActivity.class), QuizActivity.ACTIVITY_CODE);
    }

    private void startMainActivity() {
        startActivity(new Intent(getApplicationContext(), WorkspaceActivity.class));
    }

    private boolean isUsernameValid(String username) {
        return !TextUtils.isEmpty(username);
    }

    private boolean isPasswordValid(String password) {
        return !TextUtils.isEmpty(password);
    }
}

