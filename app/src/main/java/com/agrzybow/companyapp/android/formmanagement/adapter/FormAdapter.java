package com.agrzybow.companyapp.android.formmanagement.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.agrzybow.R;
import com.agrzybow.companyapp.android.formmanagement.model.FormEntity;
import com.agrzybow.companyapp.android.formmanagement.service.FormService;

import java.util.ArrayList;
import java.util.List;

public class FormAdapter extends RecyclerView.Adapter<FormHolder> {
    private List<FormEntity> entityList = new ArrayList<>();

    public FormAdapter(Context context) {
        new FormService(context).getFormList(formEntities -> {
            entityList = formEntities;
            notifyDataSetChanged();
        }, context);
    }

    @NonNull
    @Override
    public FormHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new FormHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.form, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull FormHolder holder, int position) {
        holder.bindForm(entityList.get(position));
    }

    @Override
    public int getItemCount() {
        return entityList.size();
    }
}
