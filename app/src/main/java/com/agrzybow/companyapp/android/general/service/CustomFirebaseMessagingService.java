package com.agrzybow.companyapp.android.general.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;

import androidx.core.app.NotificationCompat;

import androidx.preference.PreferenceManager;
import com.agrzybow.R;
import com.agrzybow.companyapp.android.chatmanagement.activity.ChatActivity;
import com.agrzybow.companyapp.android.chatmanagement.model.ChatMessageEntity;
import com.agrzybow.companyapp.android.chatmanagement.model.repository.ChatMessageEntityRepository;
import com.agrzybow.companyapp.android.general.activity.WorkspaceActivity;
import com.agrzybow.companyapp.android.general.model.UserEntity;
import com.agrzybow.companyapp.android.general.model.repository.UserEntityRepository;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;

import java.sql.SQLException;
import java.util.Map;

public class CustomFirebaseMessagingService extends FirebaseMessagingService {
    public static final String FIREBASE_TOKEN = "com.agrzybow.companyapp.android.firebase.token";
    private UserEntityRepository userEntityRepository;
    private ChatMessageEntityRepository chatMessageEntityRepository;
    private UserService userService;

    @Override
    public void onCreate() {
        userEntityRepository = new UserEntityRepository(getApplicationContext());
        chatMessageEntityRepository = new ChatMessageEntityRepository(getApplicationContext());
        userService = new UserService(getApplicationContext(), PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString(UserService.CLIENT_USERNAME, null));
    }

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        userService.saveToken(s, response -> {
            PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putString(FIREBASE_TOKEN, s).apply();
        });
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Map<String, String> data = remoteMessage.getData();
        String type = data.get("type");

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(getApplicationContext())
                .setColor(Color.rgb(246, 129, 33))
                .setLights(Color.rgb(246, 129, 33), 500, 1000)
                .setAutoCancel(true);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        switch (type) {
            case "ChatMessage":
                Integer id = Integer.parseInt(data.get("id"));
                String title = data.get("title");
                String content = data.get("content");
                String tag = data.get("tag");

                Intent chatIntent = new Intent(getApplicationContext(), ChatActivity.class);
                Bundle chatBundle = new Bundle();
                try {
                    UserEntity contact = userEntityRepository.getEntityById(tag);
                    chatBundle.putSerializable(ChatActivity.CONTACT_USER_OBJECT, contact);
                    chatIntent.putExtras(chatBundle);

                    ChatMessageEntity chatMessageEntity = ChatMessageEntity.builder()
                            .id(id)
                            .read(false)
                            .messageText(content)
                            .userEntityReceiver(
                                    new Gson().fromJson(
                                            PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString(UserService.CLIENT_ENTITY, null),
                                            UserEntity.class
                                    )
                            )
                            .modifyDate(System.currentTimeMillis())
                            .modifyBy(contact)
                            .build();
                    chatMessageEntityRepository.createOrUpdateEntity(chatMessageEntity);
                    PendingIntent pendingChatIntent = PendingIntent.getActivity(getApplicationContext(), contact.hashCode(), chatIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                    if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        NotificationChannel chatNotificationChannel = new NotificationChannel(tag, tag, NotificationManager.IMPORTANCE_DEFAULT);
                        notificationManager.createNotificationChannel(chatNotificationChannel);
                        notificationBuilder.setChannelId(tag);
                    }
                    Notification chatNotification = notificationBuilder
                            .setSmallIcon(R.drawable.ic_nav_communicator)
                            .setContentTitle(title)
                            .setContentText(content)
                            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                            .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                            .setContentIntent(pendingChatIntent)
                            .build();
                    notificationManager.notify(tag, 0, chatNotification);

                    Intent intent = new Intent();
                    intent.setAction(ChatActivity.RECEIVED_MESSAGE);
                    intent.putExtra(ChatActivity.MESSAGE_BUNDLE, chatMessageEntity);
                    sendBroadcast(intent);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                break;
            case "InboxItem":
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    NotificationChannel inboxItemNotificationChannel = new NotificationChannel("InboxItem", "InboxItem", NotificationManager.IMPORTANCE_DEFAULT);
                    notificationManager.createNotificationChannel(inboxItemNotificationChannel);
                    notificationBuilder.setChannelId("InboxItem");
                }
                Intent inboxIntent = new Intent(getApplicationContext(), WorkspaceActivity.class);
                inboxIntent.putExtra(WorkspaceActivity.FRAGMENT_CODE_KEY, WorkspaceActivity.INBOX_FRAGMENT_CODE);
                PendingIntent inboxPendingIntent = PendingIntent.getActivity(getApplicationContext(), 1, inboxIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                Notification inboxNotification = notificationBuilder
                        .setSmallIcon(R.drawable.ic_nav_inbox)
                        .setContentTitle(getResources().getString(R.string.new_inbox_item_notifiaction_title))
                        .setContentText(getResources().getString(R.string.new_inbox_item_notification_content))
                        .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                        .setCategory(NotificationCompat.CATEGORY_EMAIL)
                        .setContentIntent(inboxPendingIntent)
                        .build();

                notificationManager.notify(1, inboxNotification);
                break;
        }
    }
}
