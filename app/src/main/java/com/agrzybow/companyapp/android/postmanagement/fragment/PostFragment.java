package com.agrzybow.companyapp.android.postmanagement.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.agrzybow.R;
import com.agrzybow.companyapp.android.postmanagement.adapter.PostAdapter;

public class PostFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setRetainInstance(true);
        return inflater.inflate(R.layout.fragment_posts, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        RecyclerView postsRecyclerView = getActivity().findViewById(R.id.post_board);
        postsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        postsRecyclerView.setAdapter(new PostAdapter(getContext()));
    }
}
