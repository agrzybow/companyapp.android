package com.agrzybow.companyapp.android.mapmanagement.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.agrzybow.R;
import com.agrzybow.companyapp.android.mapmanagement.model.MapSpotCategoryEntity;
import com.agrzybow.companyapp.android.mapmanagement.model.MapSpotEntity;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

import java.util.Map;

public class MapSpotInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {
    private View view;
    private Map<Integer, MapSpotCategoryEntity> mapSpotCategoryEntityMap;

    public MapSpotInfoWindowAdapter(Context context, Map<Integer, MapSpotCategoryEntity> mapSpotCategoryEntityMap) {
        view = ((Activity) context).getLayoutInflater().inflate(R.layout.map_spot_info, null);
        this.mapSpotCategoryEntityMap = mapSpotCategoryEntityMap;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        MapSpotEntity mapSpotEntity = (MapSpotEntity) marker.getTag();
        if(mapSpotEntity != null) {
            MapSpotCategoryEntity mapSpotCategoryEntity = mapSpotCategoryEntityMap.get(mapSpotEntity.getCategory().getId());

            TextView titleTV = view.findViewById(R.id.map_spot_title);
            TextView descriptionTV = view.findViewById(R.id.map_spot_description);
            TextView latlngTV = view.findViewById(R.id.map_spot_latlng);
            ImageView categoryIconIV = view.findViewById(R.id.map_spot_category_icon);
            TextView categoryNameTV = view.findViewById(R.id.map_spot_category_name);
            ImageView verifiedIV = view.findViewById(R.id.map_spot_verified_icon);

            titleTV.setText(mapSpotEntity.getName());
            descriptionTV.setText(mapSpotEntity.getDescription());
            latlngTV.setText(String.format("%s, %s", mapSpotEntity.getLat(), mapSpotEntity.getLng()));
            byte[] imageBytes = Base64.decode(mapSpotCategoryEntity.getImage(), Base64.DEFAULT);
            categoryIconIV.setImageBitmap(BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length));
            categoryNameTV.setText(mapSpotCategoryEntity.getName());
            if (mapSpotEntity.getVerified()) {
                verifiedIV.setImageDrawable(view.getResources().getDrawable(R.drawable.ic_check, null));
            } else {
                verifiedIV.setImageDrawable(view.getResources().getDrawable(R.drawable.ic_cancel, null));
            }
            return view;
        }
       return null;
    }
}