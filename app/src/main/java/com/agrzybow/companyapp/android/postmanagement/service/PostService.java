package com.agrzybow.companyapp.android.postmanagement.service;

import android.content.Context;
import android.widget.Toast;
import androidx.preference.PreferenceManager;
import com.agrzybow.R;
import com.agrzybow.companyapp.android.general.service.RetrofitClientService;
import com.agrzybow.companyapp.android.general.service.UserService;
import com.agrzybow.companyapp.android.postmanagement.model.PostEntity;
import com.agrzybow.companyapp.android.postmanagement.model.api.PostAPI;
import com.agrzybow.companyapp.android.postmanagement.model.repository.PostEntityRepository;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class PostService {
    private PostAPI postAPI;
    private PostEntityRepository postEntityRepository;

    private Context context;
    private List<Disposable> disposableList = new ArrayList<>();

    public PostService(Context context) {
        this.context = context;
        postAPI = RetrofitClientService.getInstance(PreferenceManager.getDefaultSharedPreferences(context).getString(UserService.CLIENT_CREDENTIALS, null)).getService(PostAPI.class);
        postEntityRepository = new PostEntityRepository(context);
    }

    public void getPostList(Consumer<List<PostEntity>> consumer) {
        Long lastEntityDate = null;
        try {
            PostEntity newestPostEntity = postEntityRepository.getNewestEntity();
            if (newestPostEntity != null) {
                lastEntityDate = newestPostEntity.getModifyDate();
            }
        } catch (SQLException ignored) {
        }

        disposableList.add(postAPI.getAllEntities(lastEntityDate)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(postEntities -> {
                    postEntities.forEach(postEntity -> postEntity.setReadByClient(false));
                    postEntityRepository.createOrUpdateEntities(postEntities);
                    consumer.accept(postEntityRepository.getAllEntities());
                }, ex -> {
                    ex.printStackTrace();
                    Toast.makeText(context, R.string.error_server_connecting, Toast.LENGTH_LONG).show();
                    consumer.accept(postEntityRepository.getAllEntities());
                }));
    }
}
