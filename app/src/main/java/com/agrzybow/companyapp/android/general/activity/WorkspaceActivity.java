package com.agrzybow.companyapp.android.general.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import androidx.preference.PreferenceManager;
import com.agrzybow.R;
import com.agrzybow.companyapp.android.LoginActivity;
import com.agrzybow.companyapp.android.general.model.UserEntity;
import com.agrzybow.companyapp.android.general.service.UserService;
import com.agrzybow.companyapp.android.general.util.DatabaseHelper;
import com.agrzybow.companyapp.android.chatmanagement.fragment.ContactListFragment;
import com.agrzybow.companyapp.android.formmanagement.fragment.FormFragment;
import com.agrzybow.companyapp.android.inboxmanagement.fragment.InboxFragment;
import com.agrzybow.companyapp.android.mapmanagement.fragment.MapFragment;
import com.agrzybow.companyapp.android.postmanagement.fragment.PostFragment;
import com.google.android.material.navigation.NavigationView;
import com.google.gson.Gson;

import javax.annotation.Nullable;

public class WorkspaceActivity extends AppCompatActivity {
    public static final String POST_FRAGMENT_CODE = "com.agrzybow.companyapp.android.WorkspaceActivity.fragment.post";
    public static final String INBOX_FRAGMENT_CODE = "com.agrzybow.companyapp.android.WorkspaceActivity.fragment.inbox";
    public static final String COMMUNICATOR_FRAGMENT_CODE = "com.agrzybow.companyapp.android.WorkspaceActivity.fragment.communicator";
    public static final String MAP_FRAGMENT_CODE = "com.agrzybow.companyapp.android.WorkspaceActivity.fragment.map";
    public static final String FORM_FRAGMENT_CODE = "com.agrzybow.companyapp.android.WorkspaceActivity.fragment.form";
    public static final String FRAGMENT_CODE_KEY = "com.agrzybow.companyapp.android.WorkspaceActivity.fragment";

    private static final String TOOLBAR_TITLE_KEY = "com.agrzybow.companyapp.android.WorkspaceActivity.toolbar.title";
    private DrawerLayout mDrawerLayout;
    private NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mDrawerLayout = findViewById(R.id.drawer_layout);


        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorTitleText, null));
        setSupportActionBar(toolbar);

        navigationView = findViewById(R.id.nav_view);

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        UserEntity client = new Gson().fromJson(preferences.getString(UserService.CLIENT_ENTITY, null), UserEntity.class);

        View navigationHeaderView = navigationView.getHeaderView(0);

        ((TextView) navigationHeaderView.findViewById(R.id.navigation_header_client_name)).setText(client.getName());
        ((TextView) navigationHeaderView.findViewById(R.id.navigation_header_client_email)).setText(client.getEmail());
        ((TextView) navigationHeaderView.findViewById(R.id.navigation_header_client_username)).setText(client.getUsername());
        ((TextView) navigationHeaderView.findViewById(R.id.navigation_header_client_group)).setText(client.getUserGroup().getGroupText());

        if (savedInstanceState == null) {
            int firstMenuItemId = navigationView.getMenu().getItem(0).getItemId();
            navigationView.setCheckedItem(firstMenuItemId);
            navigationView.getMenu().performIdentifierAction(firstMenuItemId, 0);
        }
    }

    private void checkIntentFragment(@Nullable String code) {
        if (code != null) {
            switch (code) {
                case POST_FRAGMENT_CODE:
                    showFragment(new PostFragment(), getResources().getString(R.string.nav_post_title));
                    break;
                case INBOX_FRAGMENT_CODE:
                    showFragment(new InboxFragment(), getResources().getString(R.string.nav_inbox_board_title));
                    break;
                case COMMUNICATOR_FRAGMENT_CODE:
                    showFragment(new ContactListFragment(), getResources().getString(R.string.nav_communicator_title));
                    break;
                case MAP_FRAGMENT_CODE:
                    showFragment(new MapFragment(), getResources().getString(R.string.nav_map_title));
                    break;
                case FORM_FRAGMENT_CODE:
                    showFragment(new FormFragment(), getResources().getString(R.string.nav_forms_title));
                    break;
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        String title = getSupportActionBar().getTitle().toString();
        outState.putString(TOOLBAR_TITLE_KEY, title);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        getSupportActionBar().setTitle(savedInstanceState.getString(TOOLBAR_TITLE_KEY, getResources().getString(R.string.app_name)));
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkIntentFragment(getIntent().getStringExtra(FRAGMENT_CODE_KEY));
    }

    /**
     * Metoda zastępująca aktualny fragment wyświetlany na ekranie
     *
     * @param fragment      Nowy fragment do wyświetlenia
     * @param fragmentTitle Tytul fragmentu wyświetlany na górnym pasku
     */
    private void showFragment(Fragment fragment, String fragmentTitle) {
        mDrawerLayout.closeDrawers();
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.fragment, fragment).commit();
        getSupportActionBar().setTitle(fragmentTitle);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            mDrawerLayout.openDrawer(GravityCompat.START);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Metoda wywoływana po wybraniu tablicy ogłoszeń z bocznego menu
     *
     * @param item Obiekt wybranej opcji
     */
    public void onNavPostBoardClick(MenuItem item) {
        showFragment(new PostFragment(), getResources().getString(R.string.nav_post_title));
    }

    /**
     * Metoda wywoływana po wybraniu skrzynki odbiorczej z bocznego menu
     *
     * @param item Obiekt wybranej opcji
     */
    public void onNavInboxBoardClick(MenuItem item) {
        showFragment(new InboxFragment(), getResources().getString(R.string.nav_inbox_board_title));
    }

    /**
     * Metoda wywoływana po wybraniu komunikatora z bocznego menu
     *
     * @param item Obiekt wybranej opcji
     */
    public void onNavCommunicatorClick(MenuItem item) {
        showFragment(new ContactListFragment(), getResources().getString(R.string.nav_communicator_title));
    }

    /**
     * Metoda wywoływana po wybraniu map z bocznego menu
     *
     * @param item Obiekt wybranej opcji
     */
    public void onNavMapClick(MenuItem item) {
        showFragment(new MapFragment(), getResources().getString(R.string.nav_map_title));
    }

    /**
     * Metoda wywoływana po wybraniu formularzy z bocznego menu
     *
     * @param item Obiekt wybranej opcji
     */
    public void onNavFormsClick(MenuItem item) {
        showFragment(new FormFragment(), getResources().getString(R.string.nav_forms_title));
    }

    /**
     * Metoda wywoływana po wybraniu opcji aktualizacji danych z bocznego menu
     *
     * @param item Obiekt wybranej opcji
     */
    public void onNavRefreshClick(MenuItem item) {
        Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.fragment);
        if (currentFragment != null) {
            getSupportFragmentManager().beginTransaction().detach(currentFragment).attach(currentFragment).commit();
            Toast.makeText(getApplicationContext(), R.string.refresh_successful, Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Metoda wywoływana po wybraniu opcji wylogowania z bocznego menu
     *
     * @param item Obiekt wybranej opcji
     */
    public void onNavLogoutClick(MenuItem item) {
        DatabaseHelper.getInstance(getApplicationContext()).onLogout();

        PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit()
                .remove(UserService.CLIENT_USERNAME)
                .remove(UserService.CLIENT_CREDENTIALS)
                .remove(UserService.CLIENT_ENTITY)
                .apply();

        finish();

        Intent loginIntent = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(loginIntent);
    }
}
