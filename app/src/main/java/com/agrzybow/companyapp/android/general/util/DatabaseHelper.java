package com.agrzybow.companyapp.android.general.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;

import androidx.preference.PreferenceManager;
import com.agrzybow.companyapp.android.chatmanagement.model.ChatMessageEntity;
import com.agrzybow.companyapp.android.formmanagement.model.FormEntity;
import com.agrzybow.companyapp.android.general.service.UserService;
import com.agrzybow.companyapp.android.inboxmanagement.model.InboxItemEntity;
import com.agrzybow.companyapp.android.mapmanagement.model.MapSpotEntity;
import com.agrzybow.companyapp.android.mapmanagement.model.MapSpotCategoryEntity;
import com.agrzybow.companyapp.android.postmanagement.model.PostEntity;
import com.agrzybow.companyapp.android.general.model.UserEntity;
import com.agrzybow.companyapp.android.general.model.UserGroupEntity;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.field.DataPersisterManager;
import com.j256.ormlite.field.SqlType;
import com.j256.ormlite.field.types.ByteArrayType;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

/**
 * Klasa obsługująca działania na bazie danych w aplikacji
 */
public class DatabaseHelper extends OrmLiteSqliteOpenHelper {
    private static final int DATABASE_VERSION = 16;
    private static DatabaseHelper me;

    private DatabaseHelper(Context context, String databaseName) {
        super(context, databaseName, null, DATABASE_VERSION);
        getWritableDatabase();
    }

    public static DatabaseHelper getInstance(Context context) {
        if (me == null) {
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
            String dbName = preferences.getString(UserService.CLIENT_USERNAME, null);
            if (dbName != null) {
                DataPersisterManager.registerDataPersisters(PreferByteArrayType.getSingleton());
                me = new DatabaseHelper(context, dbName);
            }
        }
        return me;
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, UserGroupEntity.class);
            TableUtils.createTable(connectionSource, UserEntity.class);
            TableUtils.createTable(connectionSource, FormEntity.class);
            TableUtils.createTable(connectionSource, ChatMessageEntity.class);
            TableUtils.createTable(connectionSource, PostEntity.class);
            TableUtils.createTable(connectionSource, MapSpotCategoryEntity.class);
            TableUtils.createTable(connectionSource, MapSpotEntity.class);
            TableUtils.createTable(connectionSource, InboxItemEntity.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try {
            TableUtils.dropTable(connectionSource, UserGroupEntity.class, true);
            TableUtils.dropTable(connectionSource, UserEntity.class, true);
            TableUtils.dropTable(connectionSource, FormEntity.class, true);
            TableUtils.dropTable(connectionSource, ChatMessageEntity.class, true);
            TableUtils.dropTable(connectionSource, PostEntity.class, true);
            TableUtils.dropTable(connectionSource, MapSpotCategoryEntity.class, true);
            TableUtils.dropTable(connectionSource, MapSpotEntity.class, true);
            TableUtils.dropTable(connectionSource, InboxItemEntity.class, true);
            onCreate(database, connectionSource);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void onLogout() {
        DatabaseHelper.me = null;
    }

    private static class PreferByteArrayType extends ByteArrayType {
        public PreferByteArrayType() {
            super(SqlType.BYTE_ARRAY, new Class[]{byte[].class});
        }

        private static final PreferByteArrayType singleton = new PreferByteArrayType();

        public static PreferByteArrayType getSingleton() {
            return singleton;
        }
    }
}
