package com.agrzybow.companyapp.android.inboxmanagement.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.agrzybow.R;
import com.agrzybow.companyapp.android.inboxmanagement.adapter.InboxAdapter;
import com.google.android.material.tabs.TabLayout;

public class InboxFragment extends Fragment {
    private InboxAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setRetainInstance(true);
        return inflater.inflate(R.layout.fragment_inbox, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        RecyclerView inboxRecyclerView = getActivity().findViewById(R.id.inbox_board);
        inboxRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        adapter = new InboxAdapter(getContext());
        inboxRecyclerView.setAdapter(adapter);

        TabLayout tabLayout = getActivity().findViewById(R.id.inbox_tabs);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                tabSelectedListener(tab);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {}

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                tabSelectedListener(tab);

            }
        });
    }

    private void tabSelectedListener(TabLayout.Tab tab) {
        if (tab.getContentDescription() != null) {
            if (tab.getContentDescription().toString().equals(getResources().getString(R.string.all_inbox_items_tab))) {
                adapter.clearFilter();
            } else {
                if (tab.getContentDescription().toString().equals(getResources().getString(R.string.new_inbox_items_tab))) {
                    adapter.filter(false);
                } else {
                    if (tab.getContentDescription().toString().equals(getResources().getString(R.string.archive_inbox_items_tab))) {
                        adapter.filter(true);
                    }
                }
            }
        }
    }
}
