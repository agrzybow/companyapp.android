package com.agrzybow.companyapp.android.quizmanagement.model.api;

import com.agrzybow.companyapp.android.quizmanagement.model.dto.QuizDTO;
import com.agrzybow.companyapp.android.quizmanagement.model.dto.QuizUserAnswerDTO;
import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

import java.util.List;

public interface QuizAPI {
    @GET("quiz/display/{id}")
    Observable<List<QuizDTO>> getQuiz(@Path("id") String id);
    @POST("quiz/user-answer/{id}")
    Observable<QuizUserAnswerDTO> sendQuizAnswer(@Path("id") String id, @Body QuizUserAnswerDTO quizUserAnswerDTO);
}