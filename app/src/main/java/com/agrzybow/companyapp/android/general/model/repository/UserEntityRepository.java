package com.agrzybow.companyapp.android.general.model.repository;

import android.content.Context;
import com.agrzybow.companyapp.android.general.model.UserEntity;
import com.agrzybow.companyapp.android.general.util.DatabaseHelper;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.List;

public class UserEntityRepository {
    private Dao<UserEntity, String> dao;

    public UserEntityRepository(Context context) {
        try {
            this.dao = DatabaseHelper.getInstance(context).getDao(UserEntity.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public UserEntity getNewestEntity() throws SQLException {
        return dao.queryBuilder().orderBy(UserEntity.DATE_FIELD_NAME, false).queryForFirst();
    }

    public List<UserEntity> getAllEntities() throws SQLException {
        return dao.queryBuilder().orderBy(UserEntity.DATE_FIELD_NAME, false).query();
    }

    public UserEntity getEntityById(String id) throws SQLException {
        return dao.queryForId(id);
    }

    public void createOrUpdateEntity(UserEntity userEntity) throws SQLException {
        dao.createOrUpdate(userEntity);
    }

    public void createOrUpdateEntities(List<UserEntity> userEntities) throws SQLException {
        for (UserEntity userEntity : userEntities) {
            createOrUpdateEntity(userEntity);
        }
    }
}
