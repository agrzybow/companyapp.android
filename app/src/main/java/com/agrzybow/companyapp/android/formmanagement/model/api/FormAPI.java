package com.agrzybow.companyapp.android.formmanagement.model.api;

import com.agrzybow.companyapp.android.formmanagement.model.FormEntity;
import io.reactivex.Observable;
import retrofit2.http.*;

import java.util.List;

public interface FormAPI {
    @GET("form/display")
    Observable<List<FormEntity>> getAllEntities(@Query("newerThan") Long newerThan);
    @GET("form/display/{id}")
    Observable<FormEntity> getEntityById(@Path("id") Integer id);
}
