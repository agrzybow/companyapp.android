package com.agrzybow.companyapp.android.quizmanagement.service;

import android.content.Context;
import androidx.preference.PreferenceManager;
import com.agrzybow.companyapp.android.general.service.RetrofitClientService;
import com.agrzybow.companyapp.android.general.service.UserService;
import com.agrzybow.companyapp.android.quizmanagement.model.api.QuizAPI;
import com.agrzybow.companyapp.android.quizmanagement.model.dto.QuizDTO;
import com.agrzybow.companyapp.android.quizmanagement.model.dto.QuizUserAnswerDTO;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

import java.util.ArrayList;
import java.util.List;

public class QuizService {
    private QuizAPI quizAPI;
    private String clientUsername;

    private List<Disposable> disposableList = new ArrayList<>();

    public QuizService(Context context) {
        quizAPI = RetrofitClientService.getInstance(PreferenceManager.getDefaultSharedPreferences(context).getString(UserService.CLIENT_CREDENTIALS, null)).getService(QuizAPI.class);
        this.clientUsername = PreferenceManager.getDefaultSharedPreferences(context).getString(UserService.CLIENT_USERNAME, null);
    }

    public void getQuiz(Consumer<List<QuizDTO>> consumer) {
        disposableList.add(quizAPI.getQuiz(clientUsername)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(consumer, Throwable::printStackTrace));
    }

    public void sendQuizAnswer(QuizUserAnswerDTO quizUserAnswerDTO, Consumer<QuizUserAnswerDTO> consumer) {
        disposableList.add(quizAPI.sendQuizAnswer(clientUsername, quizUserAnswerDTO)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(consumer, Throwable::printStackTrace));
    }
}
