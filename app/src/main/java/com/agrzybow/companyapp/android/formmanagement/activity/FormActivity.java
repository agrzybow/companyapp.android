package com.agrzybow.companyapp.android.formmanagement.activity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import com.agrzybow.R;
import com.agrzybow.companyapp.android.formmanagement.model.FormEntity;

import java.util.Objects;

public class FormActivity extends AppCompatActivity {
    public static final String FORM_ENTITY = "com.agrzybow.companyapp.android.form.activity.form.entity";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);

        LinearLayout progressLayout = findViewById(R.id.web_view_progress_layout);
        WebView formWebView = findViewById(R.id.form_web_view);

        FormEntity formEntity = (FormEntity) Objects.requireNonNull(getIntent().getExtras()).getSerializable(FORM_ENTITY);

        if(formEntity != null) {
            ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.setDisplayHomeAsUpEnabled(true);
                actionBar.setTitle(formEntity.getName());
                actionBar.setElevation(0);
            }
            WebViewClient webViewClient = new WebViewClient() {
                @Override
                public void onPageStarted(WebView view, String url, Bitmap favicon) {
                    progressLayout.setVisibility(View.VISIBLE);
                    super.onPageStarted(view, url, favicon);
                }

                @Override
                public void onPageFinished(WebView view, String url) {
                    progressLayout.setVisibility(View.GONE);
                    super.onPageFinished(view, url);
                }
            };
            formWebView.setWebViewClient(webViewClient);
            formWebView.getSettings().setJavaScriptEnabled(true);
            formWebView.loadUrl(Objects.requireNonNull(formEntity).getUrl());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            this.finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
