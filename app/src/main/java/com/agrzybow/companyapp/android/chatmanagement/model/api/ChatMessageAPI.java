package com.agrzybow.companyapp.android.chatmanagement.model.api;

import com.agrzybow.companyapp.android.chatmanagement.model.ChatMessageEntity;
import io.reactivex.Observable;
import retrofit2.http.*;

import java.util.List;

public interface ChatMessageAPI {
    @POST("chat-message/add/{username}")
    Observable<ChatMessageEntity> saveEntity(@Path("username") String username, @Body ChatMessageEntity entityToSave);
    @GET("chat-message/display/{username}")
    Observable<List<ChatMessageEntity>> getChatMessageEntityList(@Path("username") String username, @Query("newerThan") Long newerThan);
}
