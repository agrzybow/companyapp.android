package com.agrzybow.companyapp.android.inboxmanagement.service;

import android.content.Context;
import android.widget.Toast;
import androidx.preference.PreferenceManager;
import com.agrzybow.R;
import com.agrzybow.companyapp.android.general.service.RetrofitClientService;
import com.agrzybow.companyapp.android.general.service.UserService;
import com.agrzybow.companyapp.android.inboxmanagement.model.InboxItemEntity;
import com.agrzybow.companyapp.android.inboxmanagement.model.api.InboxItemAPI;
import com.agrzybow.companyapp.android.inboxmanagement.model.repository.InboxItemEntityRepository;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class InboxService {
    private InboxItemAPI inboxItemAPI;
    private InboxItemEntityRepository inboxItemEntityRepository;

    private List<Disposable> disposableList = new ArrayList<>();
    private Context context;
    private String clientUsername;

    public InboxService(Context context) {
        this.context = context;
        this.clientUsername = PreferenceManager.getDefaultSharedPreferences(context).getString(UserService.CLIENT_USERNAME, null);
        inboxItemAPI = RetrofitClientService.getInstance(PreferenceManager.getDefaultSharedPreferences(context).getString(UserService.CLIENT_CREDENTIALS, null)).getService(InboxItemAPI.class);
        inboxItemEntityRepository = new InboxItemEntityRepository(context);
    }

    public void getInboxItemList(Consumer<List<InboxItemEntity>> consumer, Context context) {
        Long lastEntityDate = null;
        try {
            InboxItemEntity newestInboxItemEntity = inboxItemEntityRepository.getNewestEntity();
            if(newestInboxItemEntity != null) {
                lastEntityDate = newestInboxItemEntity.getModifyDate();
            }
        } catch (SQLException ignored) {}

        disposableList.add(inboxItemAPI.getAllEntities(clientUsername, lastEntityDate)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(inboxItemEntities -> {
                    inboxItemEntityRepository.createOrUpdateEntities(inboxItemEntities);
                    consumer.accept(inboxItemEntityRepository.getAllEntities());
                }, ex -> {
                    ex.printStackTrace();
                    Toast.makeText(context, R.string.error_server_connecting, Toast.LENGTH_LONG).show();
                    consumer.accept(inboxItemEntityRepository.getAllEntities());
                }));
    }

    public void setAsRead(InboxItemEntity inboxItemEntity, Consumer<InboxItemEntity> consumer) {
        disposableList.add(inboxItemAPI.markAsRead(clientUsername, inboxItemEntity.getId())
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(response -> {
            inboxItemEntityRepository.createOrUpdateEntity(response);
            consumer.accept(response);
        }, ex -> {
            ex.printStackTrace();
            Toast.makeText(context, R.string.error_server_connecting, Toast.LENGTH_LONG).show();
            consumer.accept(inboxItemEntity);
        }));
    }
}
