package com.agrzybow.companyapp.android.general.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * Klasa przechowująca informacje na temat grupy użytkowników
 */
@Data
@NoArgsConstructor
@Entity
@Table(name = "UserGroup")
public class UserGroupEntity implements Serializable {
    public static final String ID_FIELD_NAME = "id";
    public static final String GROUP_TEXT_FIELD_NAME = "groupText";
    public static final String DATE_FIELD_NAME = "modifyDate";
    public static final String MODIFY_BY_FIELD_NAME = "modifyBy";

    @Id
    @Column(name = ID_FIELD_NAME)
    private Integer id;

    /** Nazwa grupy */
    @NotNull
    @Size(min = 1)
    @Column(name = GROUP_TEXT_FIELD_NAME, nullable = false)
    private String groupText;

    @NotNull
    @Min(1)
    @Column(name = DATE_FIELD_NAME, nullable = false)
    private Long modifyDate;
}
