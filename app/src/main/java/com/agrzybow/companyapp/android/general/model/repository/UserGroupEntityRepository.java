package com.agrzybow.companyapp.android.general.model.repository;

import android.content.Context;
import com.agrzybow.companyapp.android.general.model.UserGroupEntity;
import com.agrzybow.companyapp.android.general.util.DatabaseHelper;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.List;

public class UserGroupEntityRepository {
    private Dao<UserGroupEntity, Integer> dao;

    public UserGroupEntityRepository(Context context) {
        try {
            this.dao = DatabaseHelper.getInstance(context).getDao(UserGroupEntity.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public UserGroupEntity getNewestEntity() throws SQLException {
        return dao.queryBuilder().orderBy(UserGroupEntity.DATE_FIELD_NAME, false).queryForFirst();
    }

    public List<UserGroupEntity> getAllEntities() throws SQLException {
        return dao.queryBuilder().orderBy(UserGroupEntity.DATE_FIELD_NAME, false).query();
    }

    public UserGroupEntity getEntityById(Integer id) throws SQLException {
        return dao.queryForId(id);
    }

    public void createOrUpdateEntity(UserGroupEntity userGroupEntity) throws SQLException {
        dao.createOrUpdate(userGroupEntity);
    }

    public void createOrUpdateEntities(List<UserGroupEntity> userGroupEntities) throws SQLException {
        for (UserGroupEntity userGroupEntity : userGroupEntities) {
            createOrUpdateEntity(userGroupEntity);
        }
    }
}
