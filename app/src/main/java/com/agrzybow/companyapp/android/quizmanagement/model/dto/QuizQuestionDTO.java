package com.agrzybow.companyapp.android.quizmanagement.model.dto;

import lombok.Data;

@Data
public class QuizQuestionDTO {
    private Integer id;
    private String questionText;
    private String hint;
    private QuizCategoryDTO category;
}
