package com.agrzybow.companyapp.android.chatmanagement.adapter;

import android.graphics.Typeface;
import android.view.View;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.agrzybow.R;
import com.agrzybow.companyapp.android.chatmanagement.model.ChatMessageEntity;
import com.agrzybow.companyapp.android.general.model.UserEntity;

public class ContactHolder extends RecyclerView.ViewHolder {
    private TextView nameTextView;
    private TextView lastMessageTextView;

    public ContactHolder(@NonNull View itemView) {
        super(itemView);
        nameTextView = itemView.findViewById(R.id.name_text_view);
        lastMessageTextView = itemView.findViewById(R.id.last_message_text_view);
    }

    public void bindContact(UserEntity userEntity, ChatMessageEntity lastMessage, View.OnClickListener contactClicked) {
        if (userEntity != null) {
            nameTextView.setText(userEntity.getName());
            itemView.setOnClickListener(contactClicked);
            if(lastMessage != null) {
                String messageSender;
                if (lastMessage.getModifyBy().getUsername().equals(userEntity.getUsername())) {
                    messageSender = userEntity.getName().split(" ")[0];
                } else {
                    messageSender = itemView.getResources().getString(R.string.chat_sender_you);
                }
                String lastMessageString = messageSender + ": " + lastMessage.getMessageText();
                lastMessageTextView.setText(lastMessageString);

                if (lastMessage.getRead()) {
                    lastMessageTextView.setTypeface(null);
                    lastMessageTextView.setTextColor(itemView.getResources().getColor(R.color.colorLastMessageReaded, null));
                } else {
                    lastMessageTextView.setTypeface(null, Typeface.BOLD);
                    lastMessageTextView.setTextColor(itemView.getResources().getColor(R.color.colorLastMessageUnreaded, null));
                }
            }
        }
    }
}
