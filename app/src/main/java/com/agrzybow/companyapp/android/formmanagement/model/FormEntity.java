package com.agrzybow.companyapp.android.formmanagement.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Data
@NoArgsConstructor
@Entity
@Table(name = "Form")
public class FormEntity implements Serializable {
    public static final String ID_FIELD_NAME = "id";
    public static final String NAME_FIELD_NAME = "name";
    public static final String URL_FIELD_NAME = "url";
    public static final String DATE_FIELD_NAME = "modifyDate";

    @Id
    @Column(name = ID_FIELD_NAME)
    private Integer id;

    @NotNull
    @Size(min = 1)
    @Column(name = NAME_FIELD_NAME, nullable = false)
    private String name;

    @NotNull
    @Size(min = 1)
    @Column(name = URL_FIELD_NAME, nullable = false, columnDefinition = "TEXT")
    private String url;

    @NotNull
    @Min(1)
    @Column(name = DATE_FIELD_NAME, nullable = false)
    private Long modifyDate;
}
