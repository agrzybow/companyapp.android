package com.agrzybow.companyapp.android.chatmanagement.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.agrzybow.R;
import com.agrzybow.companyapp.android.chatmanagement.activity.ChatActivity;
import com.agrzybow.companyapp.android.chatmanagement.adapter.ContactAdapter;
import com.agrzybow.companyapp.android.chatmanagement.model.ChatMessageEntity;
import com.agrzybow.companyapp.android.chatmanagement.service.ContactService;
import com.google.android.material.tabs.TabLayout;

import java.util.Objects;

public class ContactListFragment extends Fragment {
    private ContactAdapter contactAdapter;
    private BroadcastReceiver broadcastReceiver;
    private TabLayout userGroupTabs;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setRetainInstance(true);
        return inflater.inflate(R.layout.fragment_contact_list, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        RecyclerView contactsRecyclerView = Objects.requireNonNull(getActivity()).findViewById(R.id.all_contacts);
        contactsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        contactAdapter = new ContactAdapter(getContext());
        contactsRecyclerView.setAdapter(contactAdapter);

        SearchView searchView = getActivity().findViewById(R.id.search_contact_view);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                contactAdapter.filterByQuery(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                contactAdapter.filterByQuery(newText);
                return true;
            }
        });

        userGroupTabs = getActivity().findViewById(R.id.user_group_tabs);
        userGroupTabs.removeAllTabs();
        userGroupTabs.addTab(userGroupTabs.newTab().setText(R.string.all_contacts).setContentDescription("-1"));

        new ContactService(getContext()).getUserGroupList(userGroupEntities -> {
            userGroupEntities.forEach(group -> userGroupTabs.addTab(userGroupTabs.newTab().setText(group.getGroupText()).setContentDescription(Integer.toString(group.getId()))));

            userGroupTabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {
                    contactAdapter.filterByGroup(Integer.parseInt(Objects.requireNonNull(tab.getContentDescription()).toString()));
                    contactAdapter.sort();
                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {

                }

                @Override
                public void onTabReselected(TabLayout.Tab tab) {
                    contactAdapter.filterByGroup(Integer.parseInt(Objects.requireNonNull(tab.getContentDescription()).toString()));
                    contactAdapter.sort();
                }
            });
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (ChatActivity.RECEIVED_MESSAGE.equals(intent.getAction())) {
                    Bundle receivedMessageBundle = intent.getExtras();
                    if (receivedMessageBundle != null) {
                        ChatMessageEntity receivedMessage = (ChatMessageEntity) receivedMessageBundle.getSerializable(ChatActivity.MESSAGE_BUNDLE);
                        if (receivedMessage != null) {
                            contactAdapter.addNewMessage(receivedMessage);
                        }
                    }
                }
            }
        };
        Objects.requireNonNull(getContext()).registerReceiver(broadcastReceiver, new IntentFilter(ChatActivity.RECEIVED_MESSAGE));
    }

    @Override
    public void onPause() {
        super.onPause();
        Objects.requireNonNull(getContext()).unregisterReceiver(broadcastReceiver);
    }
}
