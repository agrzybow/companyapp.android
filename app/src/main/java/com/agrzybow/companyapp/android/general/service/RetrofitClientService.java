package com.agrzybow.companyapp.android.general.service;

import com.agrzybow.companyapp.android.general.util.AuthenticationInterceptor;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClientService {
    public static final int PORT = 2115;
    public static final String HOSTNAME = "192.168.0.193";

    private static RetrofitClientService me;
    private Retrofit retrofit;

    private RetrofitClientService(String credentials) {
        OkHttpClient clientBuilder = new OkHttpClient.Builder()
                .addInterceptor(new AuthenticationInterceptor(credentials))
                .build();
        retrofit = new Retrofit.Builder()
                .baseUrl("http://" + HOSTNAME + ":" + PORT + "/api/client/")
                .client(clientBuilder)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static RetrofitClientService getInstance(String credentials) {
        if(me == null) me = new RetrofitClientService(credentials);
        return me;
    }

    public <T> T getService(Class<T> serviceClass) {
        return retrofit.create(serviceClass);
    }
}
