package com.agrzybow.companyapp.android.postmanagement.model.api;

import com.agrzybow.companyapp.android.postmanagement.model.PostEntity;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

import java.util.List;

public interface PostAPI {
    @GET("post/display")
    Observable<List<PostEntity>> getAllEntities(@Query("newerThan") Long newerThan);
}
