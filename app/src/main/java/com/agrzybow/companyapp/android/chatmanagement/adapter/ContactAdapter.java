package com.agrzybow.companyapp.android.chatmanagement.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.agrzybow.R;
import com.agrzybow.companyapp.android.chatmanagement.activity.ChatActivity;
import com.agrzybow.companyapp.android.chatmanagement.model.ChatMessageEntity;
import com.agrzybow.companyapp.android.chatmanagement.service.ContactService;
import com.agrzybow.companyapp.android.general.model.UserEntity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class ContactAdapter extends RecyclerView.Adapter<ContactHolder> {
    private Context context;

    private List<UserEntity> contactList = new ArrayList<>();
    private List<UserEntity> groupContactList = new ArrayList<>();
    private List<UserEntity> allContactCopyList = new ArrayList<>();

    private HashMap<String, ChatMessageEntity> lastMessageUsernameHashMap = new HashMap<>();

    public ContactAdapter(Context context) {
        this.context = context;
        ContactService contactService = new ContactService(context);
        contactService.getUserList(userEntities -> {
            contactList = userEntities;
            groupContactList = new ArrayList<>(userEntities);
            allContactCopyList = new ArrayList<>(userEntities);

            contactService.synchronizeChatMessagesWithDatabase(result -> contactService.getLastMessages(lastMessageUsernameHashMap -> {
                this.lastMessageUsernameHashMap = lastMessageUsernameHashMap;
                sort();
            }));
        });
    }

    @NonNull
    @Override
    public ContactHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ContactHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.contact, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ContactHolder holder, int position) {
        UserEntity contactEntity = contactList.get(position);
        holder.bindContact(contactEntity, lastMessageUsernameHashMap.get(contactEntity.getUsername()), view -> {
            Intent intent = new Intent(context, ChatActivity.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable(ChatActivity.CONTACT_USER_OBJECT, contactEntity);
            intent.putExtras(bundle);
            context.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return contactList.size();
    }

    public void filterByQuery(String query) {
        if (query.isEmpty()) {
            contactList = groupContactList;
        } else {
            String queryLowerCase = query.toLowerCase();
            contactList = groupContactList.stream()
                    .filter(contact -> contact.getName().toLowerCase().contains(queryLowerCase) ||
                            contact.getName().toLowerCase().contains(queryLowerCase))
                    .collect(Collectors.toList());
        }
        notifyDataSetChanged();
    }

    public void filterByGroup(Integer groupId) {
        groupContactList = (groupId == -1) ? allContactCopyList : allContactCopyList.stream().filter(contact -> contact.getUserGroup().getId().equals(groupId)).collect(Collectors.toList());
        contactList = groupContactList;
        notifyDataSetChanged();
    }

    public void sort() {
        contactList.sort((o1, o2) -> {
            if (lastMessageUsernameHashMap.get(o1.getUsername()) == null) {
                return 1;
            }
            if (lastMessageUsernameHashMap.get(o2.getUsername()) == null) {
                return -1;
            }
            if (lastMessageUsernameHashMap.get(o1.getUsername()).getModifyDate().equals(lastMessageUsernameHashMap.get(o2.getUsername()).getModifyDate())) {
                return 0;
            }
            if (lastMessageUsernameHashMap.get(o1.getUsername()).getModifyDate() > lastMessageUsernameHashMap.get(o2.getUsername()).getModifyDate()) {
                return -1;
            }
            return 1;
        });
        notifyDataSetChanged();
    }

    public void addNewMessage(ChatMessageEntity receivedMessage) {
        lastMessageUsernameHashMap.put(receivedMessage.getModifyBy().getUsername(), receivedMessage);
        sort();
    }
}
