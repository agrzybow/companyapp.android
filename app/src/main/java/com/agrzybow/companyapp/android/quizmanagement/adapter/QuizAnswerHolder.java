package com.agrzybow.companyapp.android.quizmanagement.adapter;

import android.view.View;
import android.widget.Button;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.agrzybow.R;
import com.agrzybow.companyapp.android.quizmanagement.model.dto.QuizAnswerDTO;

public class QuizAnswerHolder extends RecyclerView.ViewHolder {
    private Button button;

    public QuizAnswerHolder(@NonNull View itemView) {
        super(itemView);
        this.button = itemView.findViewById(R.id.question_button);
    }

    public void bindAnswer(QuizAnswerDTO quizAnswerDTO, boolean answerGiven, View.OnClickListener answerListener) {
        button.setText(quizAnswerDTO.getAnswerText());
        if (answerGiven) {
            button.setOnClickListener(null);
            if (quizAnswerDTO.getIsCorrect()) {
                button.setBackground(itemView.getResources().getDrawable(R.drawable.quiz_good_answer, null));
            } else {
                button.setBackground(itemView.getResources().getDrawable(R.drawable.quiz_bad_answer, null));
            }
        } else {
            button.setOnClickListener(answerListener);
        }
    }
}
