package com.agrzybow.companyapp.android.quizmanagement.adapter;

import android.app.Activity;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import androidx.annotation.NonNull;
import androidx.preference.PreferenceManager;
import androidx.recyclerview.widget.RecyclerView;
import com.agrzybow.R;
import com.agrzybow.companyapp.android.general.model.UserEntity;
import com.agrzybow.companyapp.android.general.service.UserService;
import com.agrzybow.companyapp.android.quizmanagement.model.dto.QuizAnswerDTO;
import com.agrzybow.companyapp.android.quizmanagement.model.dto.QuizDTO;
import com.agrzybow.companyapp.android.quizmanagement.model.dto.QuizUserAnswerDTO;
import com.agrzybow.companyapp.android.quizmanagement.service.QuizService;
import io.reactivex.functions.Action;

public class QuizAnswerAdapter extends RecyclerView.Adapter<QuizAnswerHolder> {
    public static final long TIME_TO_WAIT_CORRECT_ANSWER = 2000;
    public static final long TIME_TO_WAIT_BAD_ANSWER = 10000;

    private LinearLayout hintContainer;
    private ProgressBar timeProgressBar;

    private QuizService quizService;
    private Action action;
    private QuizDTO quizDTO;
    private String clientUsername;

    private boolean answerGiven = false;

    public QuizAnswerAdapter(Activity activity, QuizDTO quizDTO, Action action) {
        this.quizDTO = quizDTO;
        this.action = action;
        this.clientUsername = PreferenceManager.getDefaultSharedPreferences(activity.getApplicationContext()).getString(UserService.CLIENT_USERNAME, null);

        this.quizService = new QuizService(activity.getApplicationContext());
        this.hintContainer = activity.findViewById(R.id.quiz_hint_container);
        this.timeProgressBar = activity.findViewById(R.id.timer_to_change);
    }

    @NonNull
    @Override
    public QuizAnswerHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new QuizAnswerHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.question, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull QuizAnswerHolder holder, int position) {
        QuizAnswerDTO answerDTO = quizDTO.getAnswers().get(position);
        holder.bindAnswer(answerDTO, answerGiven, e -> {
            giveAnswer(answerDTO);

            long millisToWait = TIME_TO_WAIT_CORRECT_ANSWER;
            timeProgressBar.setVisibility(View.VISIBLE);
            if(!answerDTO.getIsCorrect()) {
                millisToWait = TIME_TO_WAIT_BAD_ANSWER;
                hintContainer.setVisibility(View.VISIBLE);
            }

            long finalMillisToWait = millisToWait;
            new CountDownTimer(finalMillisToWait, 100) {
                @Override
                public void onTick(long millisUntilFinished) {
                    timeProgressBar.setProgress(Math.round(((float)(finalMillisToWait - millisUntilFinished)/ finalMillisToWait) * 100));
                }

                @Override
                public void onFinish() {
                    try {
                        action.run();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }.start();
        });
    }

    @Override
    public int getItemCount() {
        return quizDTO.getAnswers().size();
    }

    public void giveAnswer(QuizAnswerDTO quizAnswerDTO) {
        answerGiven = true;
        quizService.sendQuizAnswer(
                QuizUserAnswerDTO.builder()
                        .answer(quizAnswerDTO)
                        .question(quizDTO.getQuestion())
                        .modifyDate(System.currentTimeMillis())
                        .modifyBy(UserEntity.builder()
                                .username(clientUsername)
                                .build()).build(),
                response -> notifyDataSetChanged()
        );
    }
}
